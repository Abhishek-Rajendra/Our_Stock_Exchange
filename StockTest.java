package stock;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StockTest {
	
	Object[] expected_trade = new Object[4];
	
	
	@Test
	public void test() {
		Stock stocks = new Stock();
		stocks.AddOrder(2,"S","c1","s1",20,230);		//Adding orders
		stocks.AddOrder(3,"S","c5","s1",20,220);
		stocks.AddOrder(4,"B","c2","s1",25,250);
		stocks.AddOrder(5,"B","c3","s1",20,260);
		stocks.AddOrder(6,"B","c4","s2",20,260);
		stocks.AddOrder(6,"S","c5","s2",25,260);
		
		expected_trade[0] = new Trade(2, 4, "c1", "c2", "s1", 20, 230.0);		//Adding expected trades
		expected_trade[1] = new Trade(3, 4, "c5", "c2", "s1", 5, 220.0);
		expected_trade[2] = new Trade(3, 5, "c5", "c3", "s1", 15, 220.0);
		expected_trade[3] = new Trade(6, 6, "c5", "c4", "s2", 20, 260.0);

		Object[] testOutput = stocks.trade.toArray();
		assertArrayEquals(expected_trade, testOutput);		//Checking whether expected and actual values of objects are equal
		//equals() and hashCode() functions are overridden in Trade.java

	}

}
