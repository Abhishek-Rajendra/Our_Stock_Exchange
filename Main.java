package stock;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Main {

	
	public static void main(String[] args) {
		
		Stock stock=new Stock();
		
		String csvFile = "C:\\Users\\input.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] order = line.split(cvsSplitBy);

                stock.AddOrder(Integer.parseInt(order[0]),order[1],order[2],order[3],Integer.parseInt(order[4]),Double.parseDouble(order[5]));		//Adding order

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        stock.print_trade();	//Print Trade
	}

}
