package stock;

public class Trade {
	int stime;
	int btime;
	String seller;
	String buyer;
	String stock_name;
	int quantity;
	double price;
	
	public Trade(int time1,int time2,String seller,String buyer,String stock,int quantity,double price)		//Constructor to initialize all the variables
	{
		this.stime=time1;
		this.btime=time2;
		this.seller=seller;
		this.buyer=buyer;
		this.stock_name=stock;
		this.quantity=quantity;
		this.price=price;
	}
	
	//Overrides used for Junit to compare two objects of type Trade
	
	@Override
	public boolean equals(Object obj){		//check for equality
        Trade t = (Trade) obj;
        boolean status = false;
        if(this.stime==t.stime && this.btime==t.btime && this.seller==t.seller && this.buyer==t.buyer && this.stock_name==t.stock_name
                && this.quantity == t.quantity 
                && this.price == t.price){
            status = true;
        }
        return status;
    }

	@Override
	public int hashCode() {			//check for hash code
	    return this.stime*37 + this.btime*17 + this.quantity*11 + (int)this.price*7;
	}
}
