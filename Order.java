package stock;

public class Order {
	int time;
	String client;
	String stock_name;
	int quantity;
	double price;
	
	public Order(int time,String client1,String stock,int quantity,double price)	//Constructor to initialize all the variables
	{
		this.time=time;
		this.client=client1;
		this.stock_name=stock;
		this.quantity=quantity;
		this.price=price;
	}
	public void setstock(int quantity)		//Set Quantity
	{
		this.quantity=quantity;
	}

}
