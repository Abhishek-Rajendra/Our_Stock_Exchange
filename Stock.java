package stock;

import java.util.*;


public class Stock {
	
	public ArrayList<Order> sellorder = new ArrayList<Order>();
	public ArrayList<Order> buyorder = new ArrayList<Order>();
	public ArrayList<Trade> trade= new ArrayList<Trade>();
	  
/*---------------------------------------------------------------------
				Print Trades
---------------------------------------------------------------------*/	
	public void print_trade()
	{
		System.out.print("-----------------------------------------------------------------------------\n");
		System.out.print("seller_time\tbuyer_time\tSeller\tBuyer\tStock\tQuantity\tPrice\n");
		System.out.print("-----------------------------------------------------------------------------\n");
		for(int i=0;i<trade.size();i++)
		{
			System.out.print(trade.get(i).stime + "\t\t" + trade.get(i).btime + "\t\t" + trade.get(i).seller + "\t" + trade.get(i).buyer + "\t" + trade.get(i).stock_name 
			+ "\t" + trade.get(i).quantity + "\t\t" + trade.get(i).price + "\n");
		}
		if(trade.size()==0)
		{
			System.out.print("No Trades so far\n");
		}
	}

/*---------------------------------------------------------------------
				Print SellOrder
---------------------------------------------------------------------*/	
	public void print_sellorder()
	{
		System.out.print("---------------------------------------------\n");
		System.out.print("time\tSeller\tStock\tQuantity\tPrice\n");
		System.out.print("---------------------------------------------\n");
		for(int i=0;i<sellorder.size();i++)
		{
			System.out.print(sellorder.get(i).time + "\t" + sellorder.get(i).client + "\t" + sellorder.get(i).stock_name 
			+ "\t" + sellorder.get(i).quantity + "\t\t" + sellorder.get(i).price + "\n");
		}
		if(sellorder.size()==0)
		{
			System.out.print("No Sell order\n");
		}
	}
/*---------------------------------------------------------------------
				Print BuyOrder
---------------------------------------------------------------------*/	
	
	public void print_buyorder()
	{
		System.out.print("---------------------------------------------\n");
		System.out.print("time\tBuyer\tStock\tQuantity\tPrice\n");
		System.out.print("---------------------------------------------\n");
		for(int i=0;i<buyorder.size();i++)
		{
			System.out.print(buyorder.get(i).time + "\t" + buyorder.get(i).client + "\t" + buyorder.get(i).stock_name 
			+ "\t" + buyorder.get(i).quantity + "\t\t" + buyorder.get(i).price + "\n");
		}
		if(buyorder.size()==0)
		{
			System.out.print("No Buy order\n");
		}
	}

/*---------------------------------------------------------------------
				Adding New Order
---------------------------------------------------------------------*/	
	public void AddOrder(int time,String type,String client,String stock_name,int quantity,double price)
	{
		Order order= new Order(time,client,stock_name,quantity,price);

		if(type.equals("S"))		//Checking whether it is of type sell
		{
			sellorder.add(order);
			ExecuteOrders(type,sellorder.size()-1);			//Executing orders
		}
		else if(type.equals("B"))		//Checking whether it is of type buy
		{
			buyorder.add(order);
			ExecuteOrders(type,(buyorder.size()-1));		//Executing orders

		}else
		{
			System.out.print("INVALID TYPE\n");
		}
	}

/*---------------------------------------------------------------------
				Executing valid orders
---------------------------------------------------------------------*/	
	protected void ExecuteOrders(String type,int pos)
	{
		int result=0;		
		
		if(type.equals("S"))		//Checking whether it is of type sell
		{
			for(int i=0;i<buyorder.size();i++) 	//looping through buyorder when type added is sell
			{
				//Checking whether stock name are equal, seller and buyer cannot have same name, buyer's price should be greater or equal to seller's price
				// and time of sell order should be greater than or equal to buy order
				if(buyorder.get(i).stock_name.equals(sellorder.get(pos).stock_name) && !(buyorder.get(i).client.equals(sellorder.get(pos).client)) && buyorder.get(i).time <=sellorder.get(pos).time && buyorder.get(i).price >=sellorder.get(pos).price)
				{
					if(buyorder.get(i).quantity > sellorder.get(pos).quantity)	//If Buyer's quantity is higher than seller's quantity
					{
						result = buyorder.get(i).quantity - sellorder.get(i).quantity;		//Resulting Quantity
						buyorder.get(i).setstock(result);
						
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(i).time,sellorder.get(pos).client,buyorder.get(i).client,buyorder.get(i).stock_name,sellorder.get(i).quantity,sellorder.get(pos).price);
					
						sellorder.remove(pos);		//Removing the recent added object from the sellers list
						trade.add(done);
						break;
					}
					else if(buyorder.get(i).quantity < sellorder.get(pos).quantity)		//If seller's quantity is higher than buyer's quantity
					{
						result = sellorder.get(pos).quantity - buyorder.get(i).quantity;	//Resulting Quantity
						sellorder.get(pos).setstock(result);
						
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(i).time,sellorder.get(pos).client,buyorder.get(i).client,buyorder.get(i).stock_name,buyorder.get(i).quantity,sellorder.get(pos).price);
						
						buyorder.remove(i);		//Removing from buyer's list
						trade.add(done);			//Adding the trade into the array list
						i=i-1;
					}
					else		//If quantity is same for both
					{
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(i).time,sellorder.get(pos).client,buyorder.get(i).client,buyorder.get(i).stock_name,sellorder.get(i).quantity,sellorder.get(pos).price);
						
						//Removing from both the list (i.e. sellorder and buyorder)
						
						sellorder.remove(pos);
						buyorder.remove(i);
						trade.add(done);			//Adding the trade into the array list
						i=i-1;
					}
					
				}
			
			}
		}else if(type.equals("B"))		//Checking whether it is of type buy
		{	
			for(int i=0;i<sellorder.size();i++) 	//looping through sellorder when type added is sell
			{
				//Checking whether stock name are equal, seller and buyer cannot have same name, buyer's price should be greater or equal to seller's price
				// and time of buy order should be greater than or equal to sell order
				if(sellorder.get(i).stock_name.equals(buyorder.get(pos).stock_name) && !(buyorder.get(pos).client.equals(sellorder.get(i).client)) && sellorder.get(i).time <=buyorder.get(pos).time && buyorder.get(pos).price >=sellorder.get(i).price)
				{
					if(sellorder.get(i).quantity > buyorder.get(pos).quantity)		//If seller's quantity is higher than buyer's quantity
					{
						result=sellorder.get(i).quantity-buyorder.get(pos).quantity;	//Resulting Quantity
						sellorder.get(i).setstock(result);
						
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(pos).time,sellorder.get(i).client,buyorder.get(pos).client,buyorder.get(pos).stock_name,buyorder.get(pos).quantity,sellorder.get(i).price);
						
						buyorder.remove(pos);		//Removing the recent added object from the buyers list
						trade.add(done);
						break;
					}
					else if(sellorder.get(i).quantity < buyorder.get(pos).quantity)		//If Buyer's quantity is higher than seller's quantity
					{
						result=buyorder.get(pos).quantity-sellorder.get(i).quantity;	//Resulting Quantity
						buyorder.get(pos).setstock(result);
						
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(pos).time,sellorder.get(i).client,buyorder.get(pos).client,buyorder.get(pos).stock_name,sellorder.get(i).quantity,sellorder.get(i).price);
						
						sellorder.remove(i);		//Removing from seller's list
						trade.add(done);	//Adding the trade into the array list
						i=i-1;
					}
					else			//If quantity is same for both
					{
						//Adding values into Trade object done, which is later added to trade global object
						
						Trade done= new Trade(sellorder.get(pos).time,buyorder.get(pos).time,sellorder.get(i).client,buyorder.get(pos).client,buyorder.get(pos).stock_name,sellorder.get(i).quantity,sellorder.get(i).price);
						
						//Removing from both the list (i.e. seller and buyer

						buyorder.remove(pos);
						sellorder.remove(i);
						trade.add(done);	//Adding the trade into the array list
						i=i-1;
					}
					
				}
				
			}
		}
			
	}	

}
